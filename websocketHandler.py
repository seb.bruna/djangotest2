import asyncio
import websockets
import struct
import json
import serial
import random
import os 
from datetime import datetime
lock_file_path = 'websocket_server.lock'

# Lista de clientes conectados
clientes = set()

def create_lock_file():
    with open(lock_file_path, 'w') as f:
        f.write('running')

def delete_lock_file():
    os.remove(lock_file_path)

async def enviar_datos_a_clientes(datos):
    if clientes:
        # Convertir los datos a formato JSON
        datos_json = json.dumps(datos)

        # Enviar datos a todos los clientes
        await asyncio.gather(*(cliente.send(datos_json) for cliente in clientes))

async def recibir_datos(websocket, path):
    global clientes
    clientes.add(websocket)

    try:
        dataX = []
        dataY = []
        dt = []
        while True:

            # Generate random numbers instead of reading serial data
            CH1 = random.randint(0, 1023)  # Simulate a 10-bit ADC value 1
            CH2 = random.randint(0, 1023)  # Simulate a 10-bit ADC value 
            await asyncio.sleep(0.001)
            # Calculate the elapsed time in seconds
            tiempo_transcurrido = datetime.now().isoformat()
            #tiempo_transcurrido = asyncio.get_event_loop().time()

            dataX.append(CH1)
            dataY.append(CH2)
            dt.append(tiempo_transcurrido)
            if len(dataX) >= 500:
                print('sending data')
                datos = {"CH1": dataX, "CH2": dataY, "Tiempo": dt}

            
                # Send the data to all connected clients
                await enviar_datos_a_clientes(datos)
                dataX = []
                dataY = []
                dt = []
                

    except websockets.exceptions.ConnectionClosedError:
        pass
    finally:
        # Remove the client on disconnect
        clientes.remove(websocket)

start_server = websockets.serve(recibir_datos, "localhost", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()