# Django Test

## Installation

Use the requirements File.

```bash
pip install -r requirements.txt
```
## Docker
### Install docket engine to mout the database
#### Download the container:
https://drive.google.com/file/d/1sJgnSOUswrDVKU-oL9PlrnNdvF8WNr1n/view?usp=sharing

# Set up Docker's apt repository
```bash
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/raspbian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Set up Docker's APT repository:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/raspbian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```
Install the Docker packages.
```bash
 sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

```
Verify that the installation is successful by running the hello-world image:
```bash
sudo docker run hello-world
```

Restore the container:

```bash
sudo docker load -i ~/my-backup.tar
```
Verify the Docker Image
After you have loaded the Tar File, you can verify if the Image has been added to your local repository using the following command.

```bash
sudo docker images
```
Run the Restored Docker Container
```bash
sudo docker run -it my-backup:latest
```
Double check if the data base is running
```bash
sudo docker ps -a
```
Change in the file .env the IPs address, 
```bash
sudo nano webTest1/.env
```
DB_HOST: Where is the data base
WS_HOST: Raspberry pi IP
```bash
DB_HOST=192.168.1.173
DB_PORT=5050
WS_HOST=127.0.0.1
DEVICE_SERIALPORT = /dev/ttyUSB0
```


## Ngnix
```bash
sudo apt-get install nginx
```
```bash
sudo nano /etc/nginx/sites-available/default
```
```bash
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /var/www/html;
        # Add index.php to the list if you are using PHP
        index index.html index.htm;
        server_name _;
        location /ws/ {
                proxy_pass http://localhost:5050;  # Assuming WebSocket server runs on port 8765
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "Upgrade";
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
        }
}        
```
## Usage

### open two terminals and ssh to the raspberry

In both terminals the virtual env should be activate
#### Terminal 1
```bash
python PM4.py
```
#### Terminal 2
```bash
python manage.py runserver 0.0.0.0:8000
```