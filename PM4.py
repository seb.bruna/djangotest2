import asyncio
import serial_asyncio
import threading
import socket
import os

#---------------------------------------------------------------------------
def load_env_file(filepath):
    with open(filepath, 'r') as file:
        for line in file:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=', 1)
                os.environ[key] = value

# Load the .env file
load_env_file('webTest1/.env')
DEVICE_SERIALPORT = os.getenv('DEVICE')
print(DEVICE_SERIALPORT)
DEVICE_SERIALPORT
#---------------------------------------------------------------------------
class SimpleSerialProtocol(asyncio.Protocol):
    def __init__(self, broadcaster):
        super().__init__()
        self.transport = None
        self.broadcaster = broadcaster

    def connection_made(self, transport):
        self.transport = transport
        print("Connection opened")

    def data_received(self, data):
        clean_data = data.decode().replace('@', '').replace('\r\n', ',')
        
        #print(clean_data)
        self.broadcaster.broadcast(clean_data)  # Broadcast the cleaned data

    def connection_lost(self, exc):
        if exc:
            print('Error on port:', exc)
        print("Port closed")

class DataBroadcaster:
    def __init__(self, host='127.0.0.1', port=8765):
        self.host = host
        self.port = port
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clients = []

    def start_server(self):
        self.server.bind((self.host, self.port))
        self.server.listen()
        threading.Thread(target=self.accept_clients, daemon=True).start()

    def accept_clients(self):
        while True:
            client, addr = self.server.accept()
            self.clients.append(client)
            print(f"Connected to {addr}")

    def broadcast(self, message):
        for client in self.clients:
            try:
                client.sendall(message.encode('utf-8'))
            except Exception as e:
                print(f"Failed to send message: {e}")
                self.clients.remove(client)

class PowerMeter:
    def __init__(self, loop, broadcaster, DEVICE_SERIALPORT):
        BAUDRATE = 500000
        self.loop = loop
        self.port = DEVICE_SERIALPORT
        self.baudrate = BAUDRATE
        self.broadcaster = broadcaster

    async def connect(self):
        self.transport, self.protocol = await serial_asyncio.create_serial_connection(
            self.loop,
            lambda: SimpleSerialProtocol(self.broadcaster),  # Pass the broadcaster to the protocol
            self.port,
            baudrate=self.baudrate
        )
        print("Power Meter connected.")

    async def close(self):
        if self.transport:
            self.transport.close()
        print('PowerMeter connection closed.')
        

    async def startPowerMeterRun(self, powerMeterSampleRate):
        print("Starting powerMeter run:")
        if self.transport:
            # Send the command to start continuous data transmission only once
            self.transport.write(f"cpower {powerMeterSampleRate}\r".encode('UTF-8'))
            await asyncio.sleep(0.1)  # Wait a bit after sending the command

            # Keep the coroutine running to keep the asyncio loop alive
            while True:
                await asyncio.sleep(10)  # Sleep for some time to keep the loop alive without doing much work

def continuous_acquisition(pm, rate):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(pm.startPowerMeterRun(rate))
    loop.close()


async def main():
    broadcaster = DataBroadcaster()
    broadcaster.start_server()

    loop = asyncio.get_running_loop()
    pm = PowerMeter(loop, broadcaster, DEVICE_SERIALPORT)
    await pm.connect()
    thread = threading.Thread(target=continuous_acquisition, args=(pm, 4000))
    thread.start()
    try:
        await asyncio.sleep(3600)  # Keep the server running
    finally:
        await pm.close()
        thread.join()
        exit()

if __name__ == "__main__":
    asyncio.run(main())

